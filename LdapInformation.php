<?php

/**
 * This file is part of LdapExtendWebserverTokenAuthenticate plugin
 * @version 0.1.0
 */

namespace LdapExtendWebserverTokenAuthenticate;

class LdapInformation
{

    /**
     * the error when try to authenticate
     * @var string
     **/
    public $ErrorInfo = '';

    /**
     * the settings of plugin for LDAP connexion
     * @var null|array
     **/
    protected $settings;

    /**
     * the attributes to add in search
     * @var null|array
     **/
    protected $extraattributes;

    /**
     * @param array $settings
     * @param array $surveySettings
     * @param string|null $language
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * get information about user
     * $param string $username
     * @return false|array, the LDAP user or false if not connected
     */
    public function getUserInformation($username)
    {
        // Try to connect
        $ldapconn = $this->createConnection();
        if (!is_resource($ldapconn)) {
            if (empty($this->ErrorInfo)) {
                $this->addError("Unable to create a LDAP connexion.", \CLogger::LEVEL_ERROR);
            }
            return;
        }
        $searchuserattribute = $this->getSetting('searchuserattribute');
        $extrauserfilter = $this->getSetting('extrauserfilter');
        $usersearchbase = $this->getSetting('usersearchbase');
        $binddn = $this->getSetting('binddn');
        $bindpwd = $this->getSetting('bindpwd');
        $groupsearchbase = $this->getSetting('groupsearchbase');
        $groupsearchfilter = $this->getSetting('groupsearchfilter');
        $checkunicity = $this->getSetting('checkunicity');

        // in search and bind mode we first do a LDAP search from the username given
        // to foind the userDN and then we procced to the bind operation
        if (empty($binddn)) {
            // There is no account defined to do the LDAP search,
            // let's use anonymous bind instead
            $ldapbindsearch = @ldap_bind($ldapconn);
        } else {
            // An account is defined to do the LDAP search, let's use it
            $ldapbindsearch = @ldap_bind($ldapconn, $binddn, $bindpwd);
        }
        if (!$ldapbindsearch) {
            $this->addError(ldap_error($ldapconn), \CLogger::LEVEL_ERROR);
            ldap_close($ldapconn); // all done? close connection
            return false;
        }
        // Now prepare the search fitler
        if ($extrauserfilter != "") {
            $usersearchfilter = "(&($searchuserattribute=$username)$extrauserfilter)";
        } else {
            $usersearchfilter = "($searchuserattribute=$username)";
        }
        // Search for the user
        $userentry = array();
        $userdn = null;
        $usersearchbase = preg_split('/\r\n|\r|\n|;/', $usersearchbase);
        foreach ($usersearchbase as $usb) {
            $dnsearchres = ldap_search($ldapconn, $usb, $usersearchfilter, array($searchuserattribute));
            $rescount = ldap_count_entries($ldapconn, $dnsearchres);
            if ($rescount == 1) {
                $newuserentry = ldap_get_entries($ldapconn, $dnsearchres);
                $userentry[] = $newuserentry;
                $userdn = $newuserentry[0]["dn"];
            } elseif($checkunicity && $rescount > 1) {
                ldap_close($ldapconn);
                $this->addError('LDAP configuration invalid return multiple entries.', \CLogger::LEVEL_ERROR);
                return false;
            }
        }
        if ($checkunicity && count($userentry) > 1) {
            ldap_close($ldapconn);
            $this->addError('LDAP configuration invalid return multiple entries.', \CLogger::LEVEL_ERROR);
            return false;
        }
        if (empty($userentry)) {
            // if no entry or more than one entry returned
            // then deny authentication
            $this->addError('Incorrect username and/or password!');
            ldap_close($ldapconn); // all done? close connection
            return false;
        }
        if ($groupsearchbase != '' && $groupsearchfilter != '') {
            $keywords = array('$username', '$userdn');
            $substitutions = array($username, ldap_escape($userdn, "", LDAP_ESCAPE_FILTER));
            $filter = str_replace($keywords, $substitutions, $groupsearchfilter);
            $groupsearchres = ldap_search($ldapconn, $groupsearchbase, $filter);
            $grouprescount = ldap_count_entries($ldapconn, $groupsearchres);
            if ($grouprescount < 1) {
                $this->addError('Valid username but not authorized by group restriction');
                ldap_close($ldapconn); // all done? close connection
                return false;
            }
        }

        // binding to ldap server with the userDN and provided credentials
        $attributesToGet = array(
            $this->getSetting('searchuserattribute'),
            $this->getSetting('mailattribute'),
            $this->getSetting('firstnameattribute'),
            $this->getSetting('lastnameattribute'),
        );
        $otherattributes = (array) $this->getSetting('otherattributes');
        $attributesToGet = array_values(array_unique(array_filter(array_merge(
            $attributesToGet,
            $otherattributes
        ))));
        $result = ldap_search($ldapconn, $userdn, "(objectClass=*)", $attributesToGet);
        $entries = ldap_get_entries($ldapconn, $result);
        ldap_close($ldapconn);
        $user = $this->getUserInfoByEntry($entries[0]);
        $user['dn'] = $userdn;
        return $user;
    }

    /**
     * Move from an LDAP entrie to a cleaner array
     */
    private function getUserInfoByEntry($entrie)
    {
        $user = array();
        $mailattribute = strtolower($this->getSetting('mailattribute'));
        if (!empty($entrie[$mailattribute])) {
            $user['mail'] = $entrie[$mailattribute][0];
        }
        $firstnameattribute = strtolower($this->getSetting('firstnameattribute'));
        if (!empty($entrie[$firstnameattribute])) {
            $user['firstname'] = $entrie[$firstnameattribute][0];
        }
        $lastnameattribute = strtolower($this->getSetting('lastnameattribute'));
        if (!empty($entrie[$lastnameattribute])) {
            $user['lastname'] = $entrie[$lastnameattribute][0];
        }
        $otherattributes = (array) $this->getSetting('otherattributes');
        foreach ($otherattributes as $attribute) {
            $attribute = strtolower($attribute);
            if (!empty($entrie[$attribute])) {
                if ($entrie[$attribute]['count'] == 1) {
                    $user[$attribute] = $entrie[$attribute][0];
                } else {
                    unset($entrie[$attribute]['count']);
                    $user[$attribute] = implode("\n", $entrie[$attribute]);
                }
            }
        }
        return $user;
    }

    /**
     * Create connexion and return resource
     */
    private function createConnection()
    {
        $ldapserver = $this->getSetting('server');
        if (strpos($ldapserver, 'ldaps://') === false && strpos($ldapserver, 'ldap://') === false) {
            $ldapserver = 'ldap://' . $ldapserver;
        }
        $ldapport = $this->getSetting('ldapport');
        if (empty($ldapport)) {
            $ldapport = 389;
        }
        $ldapver = $this->getSetting('ldapversion');
        $ldaptls = $this->getSetting('ldaptls');
        $ldapoptreferrals  = $this->getSetting('ldapoptreferrals');
        $ldapconn = ldap_connect($ldapserver . ':' . (int) $ldapport);
        if (false == $ldapconn) {
            // LDAP connect does not connect, but just checks the URI
            $this->ErrorInfo = gT('LDAP URI could not be parsed.');
            return false;
        }
        if (empty($ldapver)) {
            // If the version hasn't been set, default = 2
            $ldapver = 2;
        }

        $connectionSuccessful = ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $ldapver);
        if (!$connectionSuccessful) {
            $this->ErrorInfo = gT('Error creating LDAP connection');
            return false;
        }
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, $ldapoptreferrals);
        if (!empty($ldaptls) && $ldaptls == '1' && $ldapver == 3 && preg_match("/^ldaps:\/\//", $ldapserver) === 0) {
            // starting TLS secure layer
            if (!ldap_start_tls($ldapconn)) {
                ldap_unbind($ldapconn); // Could not properly connect, unbind everything.
                $this->ErrorInfo = gT('Error creating TLS on LDAP connection');
                return false;
            }
        }
        return $ldapconn;
    }

    /**
     * Get a specified settings from Plugin
     * @param string
     * @return string
     */
    private function getSetting($setting)
    {
        if (empty($this->settings[$setting])) {
            return null;
        }
        if (isset($this->settings[$setting]['current'])) {
            return $this->settings[$setting]['current'];
        }
        if (isset($this->settings[$setting]['default'])) {
            return $this->settings[$setting]['default'];
        }
        return null;
    }

    /**
     * set the error when login
     * @param string message
     * @param string level for log
     * @param string function
     * @return void
     */
    private function addError($message, $level = \CLogger::LEVEL_INFO, $function = 'authenticate')
    {
        $this->ErrorInfo = gT($message);
        \Yii::log("Unable to create a LDAP connexion.", $level, 'plugin.LdapTokenAuthenticate.LdapAuthentication.' . $function);
    }
}
