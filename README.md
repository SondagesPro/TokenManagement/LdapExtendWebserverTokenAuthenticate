# LdapExtendWebserverTokenAuthenticate

Get some user indformatoion after webserver authentication using WebserverTokenAuthenticate plugin

## Documentation

Set the LDAP configuration on global settings. Actuvate and update some settings by survey in WebserverTokenAuthenticate settings.

## Support

If you need support on configuration and installation of this plugin : [create a support ticket on support.sondages.pro](https://support.sondages.pro/).

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab]( https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2023 Denis Chenu <https://sondages.pro>
- Copyright © 2023 OECD <https://oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Make a direct donation](https://support.sondages.pro/open.php?topicId=12) 
- [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
- [Donate on OpenCollective](https://opencollective.com/sondagespro) 
