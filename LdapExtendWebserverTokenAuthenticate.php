<?php

/**
 * LDAP complement after webserver authentication for token
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @copyright 2023 OECD <http://www.oecd.org>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class LdapExtendWebserverTokenAuthenticate extends PluginBase
{

    protected $storage = 'DbStorage';

    protected static $description = 'LDAP complement after webserver authentication for token. Need WebserverTokenAuthenticate plugin.';
    protected static $name = 'LdapExtendWebserverTokenAuthenticate';


    protected $settings = array(
        'server' => array(
            'type' => 'string',
            'label' => 'LDAP server',
            'help' => 'e.g. ldap://ldap.example.com or ldaps://ldap.example.com'
        ),
        'ldapport' => array(
            'type' => 'string',
            'label' => 'Port number',
            'help' => 'Default when omitted is 389',
        ),
        'ldapversion' => array(
            'type' => 'select',
            'label' => 'LDAP version',
            'options' => array('2' => 'LDAPv2', '3' => 'LDAPv3'),
            'default' => '2',
        ),
        'ldapoptreferrals' => array(
            'type' => 'boolean',
            'label' => 'Select true if referrals must be followed (use false for ActiveDirectory)',
            'default' => '0'
        ),
        'ldaptls' => array(
            'type' => 'boolean',
            'help' => 'Check to enable Start-TLS encryption, when using LDAPv3',
            'label' => 'Enable Start-TLS',
            'default' => '0'
        ),
        'usersearchbase' => array(
            'type' => 'text',
            'label' => 'Base DN for the user search operation (Search and bind)',
            'help' => 'Multiple bases may be separated by a new line',
        ),
        'extrauserfilter' => array(
            'type' => 'string',
            'label' => 'Optional extra LDAP filter to be ANDed to the basic (searchuserattribute=username) filter',
            'help' => 'Don\'t forget the outmost enclosing parentheses',
        ),
        'binddn' => array(
            'type' => 'string',
            'label' => 'DN of the LDAP account used to search for the end-user\'s DN.',
            'help' => 'Optional , an anonymous bind is performed if empty',
        ),
        'bindpwd' => array(
            'type' => 'password',
            'label' => 'Password of the LDAP account used to search for the end-user\'s DN.'
        ),
        'searchuserattribute' => array(
            'type' => 'string',
            'label' => 'Attribute to compare to the given login can be uid, cn, mail, ... login value is the server key value from WebserverTokenAuthenticate.'
        ),
        'mailattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of email address',
        ),
        'firstnameattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of first name'
        ),
        'lastnameattribute' => array(
            'type' => 'string',
            'label' => 'LDAP attribute of last name'
        ),
    );

    /** @inheritdoc */
    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeWebserverTokenAuthenticateSurveySettings');
        $this->subscribe('newWebserverTokenAuthenticateSurveySettings');
        /* Do the action */
        $this->subscribe('WebserverTokenAuthenticate');
    }

    public function beforeWebserverTokenAuthenticateSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $SurveySettingsEvent->set(
            "settings.{$this->id}",
            array(
                'name' => get_class($this),
                'settings' => array(
                    'active' => array(
                        'type' => 'select',
                        'label' => $this->gT("Find related LDAP user after webserver authentication."),
                        'options' => array(
                            1 => gT("Yes"),
                            0 => gT("No"),
                        ),
                        'htmlOptions' => array(
                            'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), gT("No"))),
                        ),
                        'current' => $this->get('active', 'Survey', $surveyId, "")
                    ),
                    'searchuserattribute' => array(
                        'type' => 'string',
                        'label' => $this->gT("LDAP attribute to compare to the server key."),
                        'htmlOptions' => array(
                            'placeholder' => CHtml::encode($this->get('searchuserattribute')),
                        ),
                        'current' => $this->get('searchuserattribute', 'Survey', $surveyId, "")
                    ),
                    'mailattribute' => array(
                        'type' => 'string',
                        'label' => $this->gT("LDAP attribute for email."),
                        'htmlOptions' => array(
                            'placeholder' => CHtml::encode($this->get('mailattribute')),
                        ),
                        'current' => $this->get('mailattribute', 'Survey', $surveyId, "")
                    ),
                    'firstnameattribute' => array(
                        'type' => 'string',
                        'label' => $this->gT("LDAP attribute for first name."),
                        'htmlOptions' => array(
                            'placeholder' => CHtml::encode($this->get('firstnameattribute')),
                        ),
                        'current' => $this->get('firstnameattribute', 'Survey', $surveyId, "")
                    ),
                    'lastnameattribute' => array(
                        'type' => 'string',
                        'label' => $this->gT("LDAP attribute for last name."),
                        'htmlOptions' => array(
                            'placeholder' => CHtml::encode($this->get('lastnameattribute')),
                        ),
                        'current' => $this->get('lastnameattribute', 'Survey', $surveyId, "")
                    ),
                )
            )
        );
    }

    public function newWebserverTokenAuthenticateSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $settings = $SurveySettingsEvent->get('settings');
        foreach ($settings as $setting => $value) {
            $this->set($setting, $value, 'Survey', $surveyId);
        }
    }

    public function WebserverTokenAuthenticate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $webserAuthEvent = $this->getEvent();
        $surveyId = $webserAuthEvent->get('surveyId');
        $ldapUserId = $webserAuthEvent->get('userid');
        $oToken = $webserAuthEvent->get('oToken');
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $settings = $this->getPluginSettings(true);
        /* replace some settings */
        if ($this->get('searchuserattribute', 'Survey', $surveyId)) {
            $settings['searchuserattribute']['current'] = $this->get('searchuserattribute', 'Survey', $surveyId);
        }
        if ($this->get('mailattribute', 'Survey', $surveyId)) {
            $settings['mailattribute']['current'] = $this->get('mailattribute', 'Survey', $surveyId);
        }
        if ($this->get('firstnameattribute', 'Survey', $surveyId)) {
            $settings['firstnameattribute']['current'] = $this->get('mailattribute', 'Survey', $surveyId);
        }
        if ($this->get('lastnameattribute', 'Survey', $surveyId)) {
            $settings['lastnameattribute']['current'] = $this->get('mailattribute', 'Survey', $surveyId);
        }
        /* Get the extra attribute caption */
        $extraAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        $otherattribute = array();
        foreach ($extraAttributes as $attribute => $attributeInfo) {
            if (trim($attributeInfo['description'])) {
                $otherattribute[$attribute] = trim($attributeInfo['description']);
            }
        }
        $settings['otherattributes']['current'] = $otherattribute;
        $LdapInformation = new LdapExtendWebserverTokenAuthenticate\LdapInformation($settings);
        $userInfo = $LdapInformation->getUserInformation($ldapUserId);
        /* We get the user : let's update token */
        if ($settings['mailattribute']['current'] && isset($userInfo[$settings['mailattribute']['current']])) {
            $oToken->mail = $userInfo[$settings['mailattribute']['current']];
        }
        if ($settings['firstnameattribute']['current'] && isset($userInfo[$settings['firstnameattribute']['current']])) {
            $oToken->mail = $userInfo[$settings['firstnameattribute']['current']];
        }
        if ($settings['lastnameattribute']['current'] && isset($userInfo[$settings['lastnameattribute']['current']])) {
            $oToken->mail = $userInfo[$settings['lastnameattribute']['current']];
        }
        foreach ($otherattribute as $attribute => $ldadapptribute) {
            if (isset($userInfo[$ldadapptribute])) {
                $oToken->setAttribute($attribute, $userInfo[$ldadapptribute]);
            }
        }
        if (!$oToken->encryptSave(false)) {
            throw new CHttpException(500, CHtml::errorSummary($oToken));
        }
        /* Add an event */
        $event = new PluginEvent('ExtendWebserverTokenAuthenticate');
        $event->set('surveyId', $surveyId);
        $event->set('oToken', $oToken);
        $event->set('userInfo', $userInfo);
        App()->getPluginManager()->dispatchEvent($event);
    }
}